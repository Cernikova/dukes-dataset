object:0
x:0,0
y:0,0
z:0,0
F_RGB:red
F_SHAPE:cylinder
object:1
x:0,0
y:0,0
z:1,1
F_RGB:red
F_SHAPE:cylinder
object:2
x:0,0
y:0,0
z:2,2
F_RGB:blue
F_SHAPE:sphere
object:3
x:0,0
y:7,7
z:0,0
F_RGB:red
F_SHAPE:cylinder
object:4
x:0,0
y:7,7
z:1,1
F_RGB:red
F_SHAPE:cylinder
object:5
x:0,0
y:7,7
z:2,2
F_RGB:blue
F_SHAPE:cylinder
object:6
x:3,3
y:3,3
z:0,0
F_RGB:gray
F_SHAPE:cylinder
object:7
x:3,3
y:3,3
z:1,1
F_RGB:red
F_SHAPE:cylinder
object:8
x:3,3
y:4,4
z:0,0
F_RGB:gray
F_SHAPE:cylinder
object:9
x:3,3
y:4,4
z:1,1
F_RGB:gray
F_SHAPE:cylinder
object:10
x:3,3
y:4,4
z:2,2
F_RGB:gray
F_SHAPE:cylinder
object:11
x:3,3
y:4,4
z:3,3
F_RGB:white
F_SHAPE:sphere
object:12
x:3,3
y:5,5
z:0,0
F_RGB:red
F_SHAPE:cylinder
object:13
x:4,4
y:5,5
z:0,0
F_RGB:blue
F_SHAPE:cylinder
object:14
x:4,4
y:5,4
z:1,0
F_RGB:gray
F_SHAPE:cylinder
object:15
x:7,7
y:0,0
z:0,0
F_RGB:red
F_SHAPE:cylinder
object:16
x:7,7
y:0,0
z:1,1
F_RGB:red
F_SHAPE:cylinder
object:17
x:7,7
y:0,0
z:2,2
F_RGB:green
F_SHAPE:sphere
object:18
x:7,7
y:7,7
z:0,0
F_RGB:red
F_SHAPE:cylinder
object:19
x:7,7
y:7,7
z:1,1
F_RGB:red
F_SHAPE:cylinder
object:20
x:7,7
y:7,7
z:2,2
F_RGB:yellow
F_SHAPE:sphere
object:21
x:0,0
y:0,0
z:1,1
F_RGB:red
F_SHAPE:tower
object:22
x:0,0
y:7,7
z:2,2
F_RGB:red-blue
F_SHAPE:tower
object:23
x:3,3
y:3,3
z:1,1
F_RGB:gray-red
F_SHAPE:tower
object:24
x:3,3
y:4,4
z:2,2
F_RGB:gray
F_SHAPE:tower
object:25
x:4,4
y:5,5
z:1,1
F_RGB:blue-gray
F_SHAPE:tower
object:26
x:7,7
y:0,0
z:1,1
F_RGB:red
F_SHAPE:tower
object:27
x:7,7
y:7,7
z:1,1
F_RGB:red
F_SHAPE:tower
object:gripper
x:5,4
y:7,6
z:6,7
