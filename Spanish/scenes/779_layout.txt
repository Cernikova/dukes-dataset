object:0
x:0,0
y:6,6
z:0,0
F_RGB:red
F_SHAPE:cylinder
object:1
x:0,0
y:6,6
z:1,1
F_RGB:red
F_SHAPE:cylinder
object:2
x:0,0
y:7,7
z:0,0
F_RGB:red
F_SHAPE:cylinder
object:3
x:0,0
y:7,7
z:1,1
F_RGB:red
F_SHAPE:cylinder
object:4
x:0,0
y:7,7
z:2,2
F_RGB:red
F_SHAPE:cylinder
object:5
x:0,0
y:7,7
z:3,3
F_RGB:white
F_SHAPE:cylinder
object:6
x:1,1
y:7,7
z:0,0
F_RGB:red
F_SHAPE:cylinder
object:7
x:1,1
y:7,7
z:1,1
F_RGB:red
F_SHAPE:cylinder
object:8
x:6,6
y:7,7
z:0,0
F_RGB:blue
F_SHAPE:cylinder
object:9
x:6,6
y:7,7
z:1,1
F_RGB:blue
F_SHAPE:cylinder
object:10
x:7,7
y:6,6
z:0,0
F_RGB:blue
F_SHAPE:cylinder
object:11
x:7,7
y:6,6
z:1,1
F_RGB:blue
F_SHAPE:cylinder
object:12
x:7,7
y:7,7
z:0,0
F_RGB:blue
F_SHAPE:cylinder
object:13
x:7,7
y:7,7
z:1,1
F_RGB:blue
F_SHAPE:cylinder
object:14
x:7,7
y:7,7
z:2,2
F_RGB:blue
F_SHAPE:cylinder
object:15
x:7,1
y:7,7
z:3,2
F_RGB:white
F_SHAPE:cylinder
object:16
x:0,0
y:6,6
z:1,1
F_RGB:red
F_SHAPE:tower
object:17
x:0,0
y:7,7
z:3,3
F_RGB:red-white
F_SHAPE:tower
object:18
x:1,1
y:7,7
z:1,1
F_RGB:red
F_SHAPE:tower
object:19
x:6,6
y:7,7
z:1,1
F_RGB:blue
F_SHAPE:tower
object:20
x:7,7
y:6,6
z:1,1
F_RGB:blue
F_SHAPE:tower
object:21
x:7,7
y:7,7
z:3,3
F_RGB:blue-white
F_SHAPE:tower
object:gripper
x:4,3
y:7,7
z:7,7
