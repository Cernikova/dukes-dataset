object:0
x:1,1
y:1,1
z:0,0
F_RGB:black
F_SHAPE:cylinder
object:1
x:1,1
y:1,1
z:1,1
F_RGB:gray
F_SHAPE:prism
object:2
x:1,1
y:6,6
z:0,0
F_RGB:black
F_SHAPE:cylinder
object:3
x:1,1
y:6,6
z:1,1
F_RGB:gray
F_SHAPE:prism
object:4
x:2,2
y:6,6
z:0,0
F_RGB:green
F_SHAPE:cylinder
object:5
x:2,7
y:6,1
z:1,0
F_RGB:gray
F_SHAPE:cylinder
object:6
x:5,5
y:6,6
z:0,0
F_RGB:black
F_SHAPE:cylinder
object:7
x:5,5
y:6,6
z:1,1
F_RGB:gray
F_SHAPE:prism
object:8
x:6,6
y:1,1
z:0,0
F_RGB:black
F_SHAPE:cylinder
object:9
x:6,6
y:1,1
z:1,1
F_RGB:gray
F_SHAPE:prism
object:10
x:6,6
y:6,6
z:0,0
F_RGB:blue
F_SHAPE:cylinder
object:11
x:2,2
y:6,6
z:1,1
F_RGB:green-gray
F_SHAPE:tower
object:gripper
x:2,2
y:6,6
z:7,7
