object:0
x:2,2
y:1,1
z:0,0
F_RGB:green
F_SHAPE:cylinder
object:1
x:2,2
y:1,1
z:1,1
F_RGB:black
F_SHAPE:cylinder
object:2
x:3,3
y:3,3
z:0,0
F_RGB:blue
F_SHAPE:cylinder
object:3
x:3,3
y:3,3
z:1,1
F_RGB:black
F_SHAPE:cylinder
object:4
x:5,5
y:4,4
z:0,0
F_RGB:white
F_SHAPE:cylinder
object:5
x:5,5
y:4,4
z:1,1
F_RGB:black
F_SHAPE:cylinder
object:6
x:6,6
y:7,7
z:0,0
F_RGB:yellow
F_SHAPE:cylinder
object:7
x:6,6
y:7,7
z:1,1
F_RGB:black
F_SHAPE:cylinder
object:8
x:6,2
y:7,1
z:2,2
F_RGB:gray
F_SHAPE:sphere
object:9
x:2,2
y:1,1
z:1,1
F_RGB:green-black
F_SHAPE:tower
object:10
x:3,3
y:3,3
z:1,1
F_RGB:blue-black
F_SHAPE:tower
object:11
x:5,5
y:4,4
z:1,1
F_RGB:white-black
F_SHAPE:tower
object:12
x:6,6
y:7,7
z:1,1
F_RGB:yellow-black
F_SHAPE:tower
object:gripper
x:6,3
y:7,7
z:7,4
